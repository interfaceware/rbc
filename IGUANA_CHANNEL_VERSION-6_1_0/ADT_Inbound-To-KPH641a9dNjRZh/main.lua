local Respell = require('respell')
local retry = require 'retry'

local function buildMsg(Inbound)
   local New = hl7.message{
      vmd  = "adt_a01.vmd",
      name = "PatientAdmit"
   }
   New.MSH = Inbound.MSH
   New.EVN = Inbound.EVN
   New.NK1 = Inbound.NK1
   New.PID = Inbound.PID
   New.OBX = Inbound.OBX
   New.PV1 = Inbound.PV1
   
   local Corrected = Respell.facilityName(New.MSH[6][1]:nodeValue())
   New.MSH[6][1] = Corrected
   return New
end

local function doParse(String)
   return hl7.parse{
      vmd  = "adt_a01.vmd",
      data = String
   }
end

local function sendMessageOut(Msg)
	local Str = Msg:S()
   net.http.post{
      url  = 'localhost:6544/validator/',
      body = Str,
      live = true
   }
end

function main(Data)
   local Msg = doParse(Data)
   local Out = buildMsg(Msg)
   --sendMessageOut(Out)
end

