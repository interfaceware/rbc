local DbConf = {
   api      = db.POSTGRES,
   name     = 'logwriter',
   user     = '',
   password = ''
}

local function getLogs(Id)
   local DbHandle = db.connect(DbConf)
   local Data = DbHandle:query{sql=[[
      SELECT messageid, body from iguanalogs.logs 
       WHERE messagetype = 'Message'
       LIMIT 10;   
      ]]
   }
   DbHandle:close()
end 

function main(Data)
   local Msg = hl7.parse{
      vmd  = "adt_a01.vmd",
      data = Data
   }
   getLogs(Msg.MSH[10]:nodeValue())
end



