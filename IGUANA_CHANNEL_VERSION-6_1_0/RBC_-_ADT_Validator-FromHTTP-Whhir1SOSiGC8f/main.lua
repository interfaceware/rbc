local function beErratic()
   math.randomseed(os.ts.time())
   math.random(100)
   math.random(100)
   local RollDice = math.random(50)
   if RollDice == 0 then
      util.sleep(15000)   
   end
end

local function validateAdt(Str)
   local Msg = hl7.parse{
      vmd  = "adt_a01.vmd",
      data = Str
   }
   if Msg.MSH[3][1]:S() == "AcmeMed" then
      return true
   end
   return false
end

   
function main(Data)
   iguana.setTimeout(300)
   beErratic()
   local Req = net.http.parseRequest{
      data = Data
   }
   if validateAdt(Req.body) then
      net.http.respond{
         body = "OK",
         code = 200
      }
   else
      net.http.respond{
         body = "Bad ADT",
         code = 400
      }
   end
   queue.push{data = Req.body}
end